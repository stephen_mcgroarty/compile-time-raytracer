#ifndef SCALAR_H
#define SCALAR_H
#include <cmath>

namespace Scalar
{
	// This variable scales all the numbers up so that
	// we can perform compile time operations on floating
	// point numbers.
	static const int64_t scale = (1<<16);

	template<int64_t v>
	struct Number 
	{
		static const int64_t val = v;	
		constexpr static const float actual_value = static_cast<float>(v) / static_cast<float>(scale);
	};


	template<int64_t v>
	struct Create
	{
		typedef Number<v*scale> result; 
	};


	template<class lhs, class rhs>
	struct Add
	{
		typedef Scalar::Number< lhs::val + rhs::val> result;
	};


	template<class lhs, class rhs>
	struct Subtract
	{
		typedef Scalar::Number< lhs::val - rhs::val> result;
	};

	template<class lhs, class rhs>
	struct Multiply
	{
		// We need to divide by the scale to make sure the value can be
		// turned back into its actual value. As the numbers are scaled
		// going into the multiplication the result will effectively be 
		// scaled 'twice'.
		typedef Scalar::Number<(lhs::val*rhs::val)/scale> result;
	};

	template<class lhs, class rhs>
	struct Divide
	{
		// We need to multiply by the scale as the numbers are already 
		// scaled going in, so the expression would look somthing like
		// this: (lhs*scale)/(rhs*scale), obviously the scales cancel
		// so we need scale^2 on the top to make sure the result is scaled.
		typedef Scalar::Number<(lhs::val*scale)/rhs::val> result;
	};
	template<class lhs>
	struct Divide<lhs,Scalar::Number<0>>
	{
		// We need to multiply by the scale as the numbers are already 
		// scaled going in, so the expression would look somthing like
		// this: (lhs*scale)/(rhs*scale), obviously the scales cancel
		// so we need scale^2 on the top to make sure the result is scaled.
		typedef Scalar::Number<0> result;
	};


	template<class num,int power>
	struct Pow
	{
		typedef typename Scalar::Multiply<
								num,
								typename Scalar::Pow<num,power-1>::result
							>::result result;
	};

	template<class num>
	struct Pow<num,1>
	{
		typedef typename Scalar::Create<1>::result result;
	};

	// Just in case!
	template<class num>
	struct Pow<num,0>
	{
		typedef typename Scalar::Create<0>::result result;
	};
	template<int power>
	struct Pow<Scalar::Number<0>,power>
	{
		typedef Scalar::Number<0> result;
	};


	// Sqrt stolen (and slightly modified) from this guy: 

	/*
		Copyright (c) 2011 Chris Lentini
		http://divergentcoder.com

		Permission is hereby granted, free of charge, to any person obtaining a copy of 
		this software and associated documentation files (the "Software"), to deal in 
		the Software without restriction, including without limitation the rights to use, 
		copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
		Software, and to permit persons to whom the Software is furnished to do so, 
		subject to the following conditions:

		The above copyright notice and this permission notice shall be included in all 
		copies or substantial portions of the Software.

		THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
		IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS 
		FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR 
		COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER 
		IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN 
		CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
	*/
	static const int64_t root = (1 << 8);

	template<int64_t V, int64_t L = 1, int64_t U = V> 
	struct ISqrt;

	template<int64_t V, int64_t R>
	struct ISqrt<V, R, R>
	{
		static const int64_t result = R;
	};

	template<int64_t L, int64_t U>
	struct ISqrt<0,L,U>
	{
		static const int64_t result = 0;
	};

	template<int64_t V, int64_t L, int64_t U>
	struct ISqrt
	{
		constexpr static const int64_t t0 = (L + U) / 2;
		constexpr static const int64_t t1 = t0 * t0;
		constexpr static const bool CND = t1 >= V;
		static const int64_t result = ISqrt<V, CND ? L : (t0 + 1),
			CND ? t0 : U>::result;
	};

	template<int64_t val>
	struct SqrtImpl
	{
		typedef typename Scalar::Create<1>::result One;
		static const bool CND = val < One::val;
		static const int64_t UPPER = CND ? (val * root) : ((val/root)+1);
		typedef Scalar::Number<ISqrt<val, 1, UPPER>::result * root> result;
	};

	template<int64_t V,bool w> 
	struct SqrtBoundsWrapper
	{ };

	template<int64_t V> 
	struct SqrtBoundsWrapper<V,true>
	{ 
			typedef typename SqrtImpl<V>::result result;
	};

	template<int64_t V> 
	struct SqrtBoundsWrapper<V,false>
	{ 
		typedef Scalar::Number<0> result;
	};


	template<class V> 
	struct Sqrt
	{
		typedef typename SqrtBoundsWrapper<V::val, !(V::val <= 0) && V::val < 85597581907790  >::result result;
	};

}


#endif
