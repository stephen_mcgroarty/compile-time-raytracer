#ifndef MATERIAL_H
#define MATERIAL_H

#include "Shapes.h"


typedef Vec3< Scalar::Number<0>,Scalar::Create<1>::result, Scalar::Number<0>> lightPosition;


template<class sphere,class ray,class distanceAlongRay>
struct PhongShade
{

	constexpr static const int64_t amibientFactor_int = 0.3f*Scalar::scale;
	constexpr static const int64_t diffuseFactor_int = 0.8f*Scalar::scale;


	typedef typename Scalar::Number<amibientFactor_int> amibientFactor;
	typedef typename Scalar::Number<diffuseFactor_int> diffuseFactor;

	// Point on the surface of the sphere = rayOrigin + rayDirection*distance
	typedef typename Vector::Add<
						typename ray::eye,
						typename Vector::Multiply<
							typename ray::direction,
							distanceAlongRay
						>::result
					>::result pointOnSphere;

	// Normal = Normalize( pointOnSphere - centerOfSphere)
    typedef typename Vector::Normalize<
    				typename Vector::Subtract< 
						pointOnSphere,
						typename sphere::position
					>::result
				>::result normal;

	typedef typename Vector::Normalize< 
				typename Vector::Subtract<
						typename sphere::position,
						lightPosition
					>::result
				>::result lightDir;



	typedef typename Vector::Subtract< 	
					Vec3< 
					 	Scalar::Number<0>,
						Scalar::Number<0>,
						Scalar::Number<0>
					>,
					lightDir
				>::result dirToLight;



	constexpr static const int64_t sDotN_int = Vector::Dot<dirToLight, normal>::result::val;
	constexpr static const int64_t sDotN_int_Clamped =sDotN_int < 0 ? 0 : sDotN_int;

	typedef typename Vector::Multiply<
				typename Vector::Multiply<
					typename sphere::col,
					diffuseFactor
				>::result,
				typename Scalar::Number<sDotN_int_Clamped>>::result diffColour;

	typedef typename Vector::Multiply<
				typename sphere::col,
				amibientFactor
			>::result ambientColour;



	typedef typename Vector::Normalize< 
				typename Vector::Add<
						typename Vector::Normalize<pointOnSphere>::result,
						dirToLight
						>::result
					>::result halfway;
            

    constexpr static const int64_t hDotN_int = Vector::Dot<halfway, normal>::result::val;
	constexpr static const int64_t hDotN_int_Clamped =hDotN_int < 0 ? 0 : hDotN_int;

	typedef typename Scalar::Number<hDotN_int_Clamped > hDotN;

    
	constexpr static const int64_t specularColour_int = Scalar::Pow<hDotN, 32>::result::val;

	constexpr static const int64_t intensity = 0.8f *Scalar::scale;


	typedef typename Vector::Add<diffColour,ambientColour>::result colourBeforeSpec;


	typedef typename Vector::Multiply<
				typename Vector::Add<
					typename Vector::Multiply<	
								typename sphere::col,
								Scalar::Number<specularColour_int>
							>::result,
						colourBeforeSpec
					>::result,
					Scalar::Number<intensity>
				>::result ouputColour;


	typedef ouputColour result;
};


template<class plane,class ray,class distanceAlongRay>
struct CheckerboardShade
{
	typedef typename Vector::Add<
					typename ray::eye,
					typename Vector::Multiply<
						typename ray::direction,
						distanceAlongRay
					>::result
				>::result pointOnPlane;

	constexpr static const int64_t black = 0.1f * Scalar::scale;
	constexpr static const int64_t white = 0.8f * Scalar::scale;

	typedef Vec3< Scalar::Number< black >,
				  Scalar::Number< black >,
				  Scalar::Number< black >
				> blackChecker;

	typedef Vec3< Scalar::Number< white >,
				  Scalar::Number< white >,
				  Scalar::Number< white >
				> whiteChecker;

	constexpr static const bool b = ((int)(pointOnPlane::X::actual_value) 
									+ (int)(pointOnPlane::Z::actual_value)) % 2;

	typedef typename IF<b, whiteChecker,blackChecker>::expression result;
};



template<class plane,class ray>
struct CheckerboardShade<plane,ray,Scalar::Create<-1>::result>
{
	typedef Vec3<Scalar::Number<0>,Scalar::Number<0>,Scalar::Number<0> > result;
};


template<class shape,class ray,class distanceAlongRay>
struct Shade
{ };


template<class Pos,class Rad,class Colour,class ray,class distanceAlongRay>
struct Shade<Sphere<Pos,Rad,Colour>,ray,distanceAlongRay>
{ 
	typedef Sphere<Pos,Rad,Colour> S;
	typedef typename PhongShade<S,ray,distanceAlongRay>::result result;
};




template<class Dis,class Norm,class Colour,class ray,class distanceAlongRay>
struct Shade<Plane<Dis,Norm,Colour>,ray,distanceAlongRay>
{ 
	typedef Plane<Dis,Norm,Colour> P;
	typedef typename CheckerboardShade< P, ray, distanceAlongRay >::result result;
};


#endif