#ifndef SCENE_H
#define SCENE_H
#include "Shapes.h"
#include "Material.h"

#define MAX_RAYS 2

template<int index>
struct SceneObject { };

template<>
struct SceneObject<1> 
{ 
	constexpr static const int64_t y = 0.2f *Scalar::scale;

	typedef Vec3<Scalar::Create<0>::result,
					Scalar::Number<y>,
					Scalar::Create<-3>::result> spherePosition;
	typedef Vec3<Scalar::Create<1>::result,
					Scalar::Create<0>::result,
					Scalar::Create<0>::result> sphereColour;

	typedef Sphere<spherePosition,Scalar::Create<1>::result,sphereColour> object;
};


template<>
struct SceneObject<0> 
{ 
	typedef Vec3<Scalar::Number<0>,
					Scalar::Create<1>::result,
					Scalar::Number<0>> planeNormal;

	typedef typename Scalar::Create<1>::result planeDistance;


	typedef Vec3<Scalar::Create<0>::result,
					Scalar::Create<0>::result,
					Scalar::Create<1>::result> planeColour;

	typedef Plane<planeDistance,planeNormal,planeColour> object;
};



template<class previousRay,class point, class normal,bool hit>
struct SecondaryRay
{
	constexpr const static int64_t bias = 0.0001f*Scalar::scale;
	typedef typename Vector::Normalize<typename Vector::Reflect<typename previousRay::direction,normal>::result>::result direction;
	typedef typename Vector::Add<point,typename Vector::Multiply<normal,Scalar::Number<bias>>::result>::result eye;
};


template<class previousRay,class point, class normal>
struct SecondaryRay<previousRay,point,normal,false>
{
	typedef Vec3<Scalar::Number<0>,Scalar::Number<0>,Scalar::Number<0>> direction;
	typedef Vec3<Scalar::Number<0>,Scalar::Number<0>,Scalar::Number<0>>  eye;
};

template<class ray,class point,class normal,bool didHit,int inIndex,int rayNumber>
struct FireAdditionalRay { };
	


struct NullHit
{
	typedef Scalar::Create<-1>::result result;
	typedef Vec3<Scalar::Number<0>,Scalar::Number<0>,Scalar::Number<0>>  point;
};


template<class ray, int index,bool>
struct CheckIfConditionIsTrue
{};


template<class ray, int index>
struct CheckIfConditionIsTrue<ray,index,true>
{
	typedef CheckIntersection<ray, typename SceneObject<index>::object> result;

};

template<class ray, int index>
struct CheckIfConditionIsTrue<ray,index,false>
{
	typedef NullHit result;
};




template<class ray,bool hit = true,int number =0,int inIndex =-1>
struct SceneIntersection
{
	typedef typename CheckIfConditionIsTrue<ray, 0,inIndex != 0>::result planeResult_raw;
	typedef typename CheckIfConditionIsTrue<ray, 1,inIndex != 1>::result sphereResult_raw;

	typedef typename planeResult_raw::result planeResult;
	typedef typename sphereResult_raw::result sphereResult;



	constexpr static const bool hitSphere = sphereResult::actual_value > 0.0f && inIndex != 1;
	constexpr static const bool hitPlane  = planeResult::actual_value > 0.0f;

	typedef typename IF<hitSphere, 
						sphereResult,
						planeResult>::expression intersectionResult;

	typedef typename IF<hitSphere, 
						sphereResult_raw,
						planeResult_raw>::expression intersectionResult_Raw;


	constexpr static const int index = hitSphere ? 1 : 0;

	typedef typename FireAdditionalRay< ray,
							  typename intersectionResult_Raw::point,
								typename GetNormal<typename intersectionResult_Raw::point,
											       typename SceneObject<index>::object>::normal,
							  hitSphere || hitPlane,
							  index,
							  number
							>::colour additionalRayColour;
	
		

	typedef typename IF<inIndex != index , 
						additionalRayColour,
						Vec3<Scalar::Number<0>,Scalar::Number<0>,Scalar::Number<0>>
					>::expression additionalColours;

	constexpr static const int64_t reflectivity = 0.2f * Scalar::scale;
			
	typedef typename Vector::Multiply< additionalColours,
									   typename Scalar::Number<reflectivity> 
									 >::result reflectedCol;
	
	typedef typename Vector::Add<
							typename Shade<typename SceneObject<index>::object,
								ray,
								intersectionResult
							>::result,
							reflectedCol
						>::result colour;
};


template<class ray,class point,class normal,int inIndex,int rayNumber>
struct FireAdditionalRay<ray,point,normal,true,inIndex,rayNumber>
{
	typedef typename SceneIntersection< 
				SecondaryRay< ray,point, normal, true>,
				true,
				rayNumber+1,
				inIndex
			>::colour colour;
};


// If we didn't hit anything last time 
template<class ray,class point,class normal,int inIndex,int rayNumber>
struct FireAdditionalRay<ray,point,normal,false,inIndex,rayNumber>
{
	typedef Vec3<Scalar::Number<0>,Scalar::Number<0>,Scalar::Number<0>> colour;	
};

// If we hit the ray limit.
template<class ray,int inIndex>
struct SceneIntersection<ray,true,MAX_RAYS,inIndex>
{	
	typedef Vec3<Scalar::Number<0>,Scalar::Number<0>,Scalar::Number<0>> colour;
};

// If we didn't hit anything.
template<class ray,int number,int inIndex>
struct SceneIntersection<ray,false,number,inIndex>
{
	typedef Vec3<Scalar::Number<0>,Scalar::Number<0>,Scalar::Number<0>> colour;	
};

#endif