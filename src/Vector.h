#ifndef VECTOR_H
#define VECTOR_H
#include <cstdint>
#include "Scalar.h"
#include <stdio.h>

template<class ix,class iy,class iz>
struct Vec3
{
	typedef ix X;
	typedef iy Y;
	typedef iz Z;
};


namespace Vector
{



	template<class V>
	struct Print
	{
		static void Dump()
		{
			printf("(%f, %f, %f)\n",V::X::actual_value,V::Y::actual_value,V::Z::actual_value );
		}
	};


	// Add two vectors
	template<class V1,class V2>
	struct Add
	{
		typedef Vec3< typename Scalar::Add<typename V1::X,typename V2::X>::result,
						typename Scalar::Add<typename V1::Y,typename V2::Y>::result,
						typename Scalar::Add<typename V1::Z,typename V2::Z>::result > result;
	};

	// Subtract two vectors
	template<class V1,class V2>
	struct Subtract
	{
		typedef Vec3< typename Scalar::Subtract<typename V1::X,typename V2::X>::result,
						typename Scalar::Subtract<typename V1::Y,typename V2::Y>::result,
						typename Scalar::Subtract<typename V1::Z,typename V2::Z>::result > result;
	};



	// Dot product between vectors
	template<class V1,class V2>
	struct Dot
	{
		typedef Scalar::Number< Scalar::Multiply<typename V1::X,typename V2::X>::result::val+ 
					 Scalar::Multiply<typename V1::Y,typename V2::Y>::result::val +
					 Scalar::Multiply<typename V1::Z,typename V2::Z>::result::val > result;
	};


	template<class V1, class V2>
	struct Cross
	{
		typedef Vec3< 
				typename Scalar::Subtract<
					typename Scalar::Multiply<typename V1::Y,typename V2::Z>::result,
					typename Scalar::Multiply<typename V1::Z,typename V2::Y>::result
					>::result,
				typename Scalar::Subtract<
					typename Scalar::Multiply<typename V1::Z,typename V2::X>::result,
					typename Scalar::Multiply<typename V1::X,typename V2::Z>::result
					>::result,
				typename Scalar::Subtract<
					typename Scalar::Multiply<typename V1::X,typename V2::Y>::result,
					typename Scalar::Multiply<typename V1::Y,typename V2::X>::result 
					>::result
				>result;
	};

	template<class V1, class S1>
	struct Multiply
	{
		typedef Vec3< typename Scalar::Multiply<typename V1::X,S1>::result,
						typename Scalar::Multiply<typename V1::Y,S1>::result,
						typename Scalar::Multiply<typename V1::Z,S1>::result >result;
	};

	template<class V>
	struct Length
	{
		typedef typename Scalar::Add<
					typename Scalar::Add<	
							typename Scalar::Multiply<typename V::X,typename V::X>::result,
							typename Scalar::Multiply<typename V::Y,typename V::Y>::result
							>::result,
						typename Scalar::Multiply<typename V::Z,typename V::Z>::result
						>::result result;
	};


	template<class V1>
	struct Normalize
	{
		typedef typename Length<V1>::result L;

		constexpr static const int64_t length = L::val;
		constexpr static const int64_t x = length == 0 || V1::X::val == 0 ? V1::X::val : Scalar::Divide<typename V1::X,typename Scalar::Sqrt<L>::result>::result::val;
		constexpr static const int64_t y = length == 0 || V1::Y::val == 0 ? V1::Y::val : Scalar::Divide<typename V1::Y,typename Scalar::Sqrt<L>::result>::result::val;
		constexpr static const int64_t z = length == 0 || V1::Z::val == 0 ? V1::Z::val : Scalar::Divide<typename V1::Z,typename Scalar::Sqrt<L>::result>::result::val;
		

		typedef Vec3< Scalar::Number<x>, Scalar::Number<y>, Scalar::Number<z> > result;
		typedef Print<result> dumpResult;

		static void Dump()
		{
			printf("length = %lld\n",length );
			printf("x = %lld\n",x );
			printf("y = %lld\n",y );
			printf("z = %lld\n",z );

			printf("result: ");
			dumpResult::Dump();
		}
	};


	// Reflect vector around 'normal'
	template<class vec,class normal>
	struct Reflect
	{
	    typedef typename Vector::Normalize<
	    			typename Vector::Subtract<
	    					vec,
	    					typename Vector::Multiply<
	    						typename Vector::Multiply<
	    							normal,
	    							Scalar::Create<2>::result
	    						>::result,
	    						typename Vector::Dot<vec,normal>::result
	    					>::result
	    				>::result
	    			>::result result;
	};

}

#endif