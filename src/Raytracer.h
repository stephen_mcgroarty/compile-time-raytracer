#ifndef RAYTRACER_H
#define RAYTRACER_H
#include "Camera.h"
#include "Material.h"
#include "Scene.h"

typedef Scalar::Create<300>::result WIDTH;
typedef Scalar::Create<300>::result HEIGHT;

typedef Vec3< Scalar::Number<0>,Scalar::Create<1>::result, Scalar::Number<0>> lightPosition;


template<class x,class y>
struct Ray
{
       constexpr static const float alpha = Camera<WIDTH,HEIGHT>::fov * ((x::actual_value - Camera<WIDTH,HEIGHT>::HalfWidth::actual_value)/Camera<WIDTH,HEIGHT>::HalfWidth::actual_value);
       constexpr static const float beta  = Camera<WIDTH,HEIGHT>::fov * ((y::actual_value - Camera<WIDTH,HEIGHT>::HalfHeight::actual_value)/Camera<WIDTH,HEIGHT>::HalfHeight::actual_value);
       constexpr static const int64_t alphai = alpha*(float)Scalar::scale;
       constexpr static const int64_t betai = beta*(float)Scalar::scale;

       typedef Scalar::Number< alphai> Alpha;
       typedef Scalar::Number< betai> Beta;

       typedef typename Vector::Normalize<
                               typename Vector::Subtract< 
                                       typename Vector::Add< 
                                               typename Vector::Multiply<Camera<WIDTH,HEIGHT>::U,Alpha>::result, 
                                               typename Vector::Multiply<Camera<WIDTH,HEIGHT>::V,Beta>::result
                                         >::result, 
                                         Camera<WIDTH,HEIGHT>::W
                               >::result
                       >::result direction;
       
       typedef Camera<WIDTH,HEIGHT>::Eye eye;
};




template<class x,class y>
struct CalculatePrimaryRay
{
	typedef Ray<x,y> result;
};


template<class x,class y>
struct InitRow
{
	typedef typename CalculatePrimaryRay<x,y>::result primaryRay;
	typedef SceneIntersection<primaryRay> rayCast;
	typedef typename rayCast::colour outColour;

	typedef typename Scalar::Subtract<x,typename Scalar::Create<1>::result>::result xMinusOne;

	typedef InitRow< xMinusOne,y> nextXResult;


	static inline void Save(float * canvas, int width,int height)
	{
		const int index = width*y::actual_value*3 + x::actual_value*3;
		canvas[index] = outColour::X::actual_value;
		canvas[index+1] = outColour::Y::actual_value;
		canvas[index+2] = outColour::Z::actual_value;

		nextXResult::Save(canvas,width,height);
	}
};


template<class y>
struct InitRow<Scalar::Number<0>,y>
{

	typedef typename CalculatePrimaryRay<Scalar::Number<0>,y>::result primaryRay;
	typedef typename SceneIntersection<primaryRay>::colour outColour;

	static inline void Save(float * canvas, int width,int height)
	{
		const int index = width*y::actual_value*3;
		canvas[index] = outColour::X::actual_value;
		canvas[index+1] = outColour::Y::actual_value;
		canvas[index+2] = outColour::Z::actual_value;

	}
};

template<class x,class y>
struct Init
{

	typedef typename CalculatePrimaryRay<x,y>::result primaryRay;
	typedef typename SceneIntersection<primaryRay>::colour outColour;

	typedef typename Scalar::Subtract<y,typename Scalar::Create<1>::result>::result yMinusOne;
	typedef typename Scalar::Subtract<x,typename Scalar::Create<1>::result>::result xMinusOne;
	
	typedef InitRow< xMinusOne,y> nextXResult;
	typedef Init< x, yMinusOne> nextYResult;

	static inline void Save(float * canvas, int width,int height)
	{
		const int index = width*y::actual_value*3 + x::actual_value*3;

		canvas[index] = outColour::X::actual_value;
		canvas[index+1] = outColour::Y::actual_value;
		canvas[index+2] = outColour::Z::actual_value;

		nextXResult::Save(canvas,width,height);
		nextYResult::Save(canvas,width,height);
	}
};


template<class x>
struct Init<x,Scalar::Number<0>>
{
	typedef typename CalculatePrimaryRay<x,Scalar::Number<0>>::result primaryRay;
	typedef typename SceneIntersection<primaryRay>::colour outColour;

	typedef typename Scalar::Subtract<x,typename Scalar::Create<1>::result>::result xMinusOne;

	typedef InitRow< xMinusOne,Scalar::Create<0>::result> nextXResult;
	


	static inline void Save(float * canvas, int width,int height)
	{
		const int index = x::actual_value*3;
		canvas[index] = outColour::X::actual_value;
		canvas[index+1] = outColour::Y::actual_value;
		canvas[index+2] = outColour::Z::actual_value;

		nextXResult::Save(canvas,width,height);
	}
};

#endif