#ifndef CAMERA_H
#define CAMERA_H
#include "Vector.h"
#include "Utils.h"

template<class Width,class Height>
struct Camera
{
	constexpr static const float fov = M_PI/4.0f;
	constexpr static const int64_t fovi = fov * Scalar::scale;
	constexpr static const int64_t halfWidthi = (Width::actual_value/2.0f)*Scalar::scale;
	constexpr static const int64_t halfHeighti =(Height::actual_value/2.0f)*Scalar::scale;

	typedef Scalar::Number< halfWidthi> HalfWidth;
	typedef Scalar::Number< halfHeighti> HalfHeight;

	typedef Scalar::Number<fovi> FOV;
	// UP vector
	typedef Vec3<Scalar::Create<0>::result, Scalar::Create<1>::result, Scalar::Create<0>::result> UP;
	
	// Eye vector
	typedef Vec3<Scalar::Create<0>::result, Scalar::Create<0>::result, Scalar::Create<0>::result> Eye;
	
	// At vector
	typedef Vec3<Scalar::Create<0>::result, Scalar::Create<0>::result, Scalar::Create<-1>::result> At;

	typedef Vec3<Scalar::Create<0>::result, Scalar::Create<0>::result, Scalar::Create<1>::result> W;

	// Normalize(Cross(W,UP)*FOV)
	typedef typename Vector::Normalize< 
							typename Vector::Multiply< 
								typename Vector::Cross< W, UP>::result, 
								FOV>::result 
							>::result U;

	// Normalize(Cross(W,U)*FOV)
	typedef  Vector::Normalize< 
				typename Vector::Multiply< 
						typename Vector::Cross< W, U>::result, 
						FOV>::result> Vtemp;

	typedef typename Vtemp::result V;
};

#endif