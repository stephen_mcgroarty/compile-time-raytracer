#ifndef SHAPES_H
#define SHAPES_H
#include "Vector.h"
#include "Scalar.h"


template<class Pos,class Rad,class Colour>
struct Sphere
{
	typedef Pos position;
	typedef Rad radius; 
	typedef	typename Scalar::Multiply<radius,radius>::result radiusSquared;

	typedef Colour col;
};


template<class Dis,class Norm,class Colour>
struct Plane
{
	typedef Dis distance;
	typedef Norm normal; 
	typedef Colour col;
};



template<class R, class S>
struct CheckIntersection
{ };


// Check Sphere intersection
template<class R,class Pos,class Rad,class Colour>
struct CheckIntersection<R,Sphere<Pos,Rad,Colour>>
{
	typedef Sphere<Pos,Rad,Colour> S;
	typedef typename Vector::Subtract<typename R::eye, typename S::position>::result VectorToSphere;

	// B = -2 * Dot( RayDirection, VectorToSphere )
	typedef typename Scalar::Multiply<
					Scalar::Create<-2>::result,
					typename Vector::Dot<typename R::direction, VectorToSphere
					>::result>::result B;


	// C  = toSphere^2 - radius^2
	typedef typename Scalar::Subtract< 
							typename Vector::Dot< VectorToSphere, VectorToSphere>::result,
							typename S::radiusSquared
						>::result C;

	// B*B
	typedef typename Scalar::Multiply<B, B>::result BSquared;

	// 4*C
	typedef typename Scalar::Multiply< Scalar::Create<4>::result, C>::result C4;

	//discriminant = B*B - 4*C
	typedef typename Scalar::Subtract<BSquared,C4>::result discriminant;

	// B - sqrt(discriminant)
	typedef typename Scalar::Subtract< 
							B,
							typename Scalar::Sqrt<discriminant>::result 
							>::result bMinusDisc;
	
	// res = (B -Sqrt(discriminant)/2)
	typedef typename Scalar::Divide<bMinusDisc,typename Scalar::Create<2>::result>::result res;

	// if disciminant < 0 then -1
	typedef typename IF< discriminant::actual_value < 0,
						typename Scalar::Create<-1>::result, 
						typename Scalar::Create<0>::result
						>::expression isLessThanZero;

	// if disciminant >= 0 then (B -Sqrt(discriminant)/2)
	typedef typename IF< discriminant::actual_value >= 0, res, typename Scalar::Create<0>::result>::expression isEqualToZero;

	// Add the two, as one will be 0
	typedef typename Scalar::Add<isLessThanZero,isEqualToZero>::result result;	


	// Point of the intersection
	typedef typename Vector::Add<
				typename R::eye,
				typename Vector::Multiply<
					typename R::direction,
					result
				>::result
			>::result point;
};


// Check plane intersection
template<class R,class Dis,class Norm,class Colour>
struct CheckIntersection<R,Plane<Dis,Norm,Colour>>
{

	typedef Plane<Dis,Norm,Colour> P;
	// Distance dot direction
	typedef typename Vector::Dot<typename R::direction, typename P::normal>::result DDotN;

	typedef typename Scalar::Multiply< 
						typename Scalar::Add< 
							typename Vector::Dot<typename R::eye,typename P::normal>::result,
							typename P::distance
							>::result,
						typename Scalar::Create<-1>::result
						>::result top;

	typedef typename Scalar::Divide<top,DDotN>::result t;


	typedef typename IF< t::actual_value <= 0,
						typename Scalar::Create<-1>::result, 
						typename Scalar::Create<0>::result
						>::expression isLessThanOrEqualToZero;

	typedef typename IF< t::actual_value <= 0, 
						typename Scalar::Create<0>::result,
						t
						>::expression isEqualToZero;

	typedef typename Scalar::Add<
							isLessThanOrEqualToZero,
							isEqualToZero
						>::result result;


	// Point of the intersection
	typedef typename Vector::Add<
				typename R::eye,
				typename Vector::Multiply<
					typename R::direction,
					result
				>::result
			>::result point;
};



template<class point,class object>
struct GetNormal
{};


template<class point,class Pos,class Rad,class Colour>
struct GetNormal<point,Sphere<Pos,Rad,Colour>>
{
	typedef Sphere<Pos,Rad,Colour> sphere;

    typedef typename Vector::Normalize<
    				typename Vector::Subtract< 
						point,
						typename sphere::position
					>::result
				>::result normal;
};
    
template<class point,class Dis,class Norm,class Colour>
struct GetNormal<point,Plane<Dis,Norm,Colour>>
{
    typedef typename Plane<Dis,Norm,Colour>::normal normal;
};
#endif