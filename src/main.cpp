#include <stdio.h>
#include "lodepng.h"
#include <algorithm>
#include "Raytracer.h"

// Save the buffer as a png, takes in a RGB float with expected range of 0.0f-1.0f
void savePNG(const char * filename,float *buffer,int width, int height)
{
	const int size = width*height*3;
	unsigned char imageAsChar[size];

	// Convert the buffer to a char array of the same size
    for (int y = 0; y < height; ++y)
    {
        for (int x =0; x < width; ++x)
        {
            int index = y*width*3 + x*3;
            imageAsChar[index]   = buffer[index]*255.0f;
            imageAsChar[index+1] = buffer[index+1]*255.0f;
            imageAsChar[index+2] = buffer[index+2]*255.0f;
        }
	}
	
	// Encode as RGB and save
	lodepng_encode24_file(filename,imageAsChar,width,height);
}



int main(int argc, char * argv[])
{	
	// Initalize blank canvas
	const int size = WIDTH::actual_value*HEIGHT::actual_value*3*4;
	float canvas[size];


	typedef Init< WIDTH,HEIGHT> result;


	result::Save(canvas,WIDTH::actual_value,HEIGHT::actual_value);


	savePNG("Result.png",canvas,WIDTH::actual_value,HEIGHT::actual_value);
}
