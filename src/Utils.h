#ifndef UTILS_H
#define UTILS_H

template<bool condition, class TrueResult, class FalseResult>
struct IF;

template<class TrueResult, class FalseResult>
struct IF<true,TrueResult,FalseResult>
{
	typedef TrueResult expression;
};


template<class TrueResult, class FalseResult>
struct IF<false,TrueResult,FalseResult>
{
	typedef FalseResult expression;
};


#endif 